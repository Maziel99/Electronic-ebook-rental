<!DOCTYPE html>
<?php
    session_start();
?>
<html lang="PL-pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Internetowa Wypożyczalnia E-booków</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <header>
    <h1>Home</h1>
    </header>
    <nav class="navbar sticky-top navbar-dark navbar-expand-lg" style="background-color: #444444;">
            <a class="navbar-brand" href="#">
            <div class="d-inline-block align-bottom baner"><img src="brand.png" alt="" height="50" width="50"></div>
                <div class="d-inline-block align-bottom baner">Internetowa <span class="title">Wypożyczalnia</span> E-booków</div>
            </a>
            <buttton class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#hambmenu" aria-controls="hambmenu" aria-expanded="false" aria-label="Navigation button">
                <span class="navbar-toggler-icon"></span>
            </buttton>
            <div class="collapse navbar-collapse justify-content-end" id="hambmenu">
            <div class="navbar-nav">

            <a class="nav-link" href="adminHome.php">Strona główna</a>

            <a class="nav-link" href="index.php">Logowanie</a>

            <a class="nav-link" href="rejestracja.php">Rejestracja</a>

            <?php
                if(isset($_SESSION['Authenticated']) && ($_SESSION['Authenticated'] == 1)){
            ?>
            <a class="nav-link login" href="wylogowanie.php?wyloguj">Wylogowanie</a>
            <?php
            }
            ?>
            </div>
        </div>
        </nav>
    <main>

<div class="container-fluid">

    <div class="row">
    <table class='tabela' id="tabela">
        <tr>
       <th>Nr</th>  <th>Klient</th> <th>Tytuł</th> <th>Data wypożyczenia</th> <th>Termin wypożyczenia</th><th>Akcje</th>
       </tr>
        </div>
    <?php
    
        error_reporting(E_ALL);
        ini_set('display_errors', 'On');
        
        include "DBconnection.php";
        
        $query = "
        BEGIN
        wczytajwypozyczenia;
        END;";
        
        $c = oci_connect($username, $password, $database, null, OCI_SYSDBA);
        if (!$c) {
            $m = oci_error();
            trigger_error('Could not connect to database: '. $m['message'], E_USER_ERROR);
        }
        
        $s = oci_parse($c, $query);
        if (!$s) {
            $m = oci_error($c);
            trigger_error('Could not parse statement: '. $m['message'], E_USER_ERROR);
        }
        $r = oci_execute($s);
        if (!$r) {
            $m = oci_error($s);
            trigger_error('Could not execute statement: '. $m['message'], E_USER_ERROR);
        }
        

        $i = 1; 
		while (($row = oci_fetch_array($s, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
			echo "<tr>\n";
			foreach ($row as $item) {
				echo "<td>";
				echo $item!==null?htmlspecialchars($item, ENT_QUOTES|ENT_SUBSTITUTE):"&nbsp;";
				echo "</td>\n";
				
			}
            if(isset($_SESSION['Authenticated']) && ($_SESSION['Authenticated'] == 1)){
            echo "<td><button id='usun' type='button' onclick=usunWypozyczenie(".$i.")>Usuń</button></td>\n";
            }
			echo "</tr>\n";
			$i++;
            
            }
        echo "</table>\n";
    ?>
    </div>


    <form method="post" action="usunWypozyczenie.php" id="formularz1">
	</form>
    </div>
    <script>
		for(i = 1; i < document.getElementById("tabela").rows.length; i++)
			{
				document.getElementById("tabela").rows[i].cells[0].id="wiersz" + i;
				
			}
			
		function usunWypozyczenie(x){
			let wiersz = document.getElementById("wiersz" + x).innerText;
			let formularz = document.getElementById("formularz1");
			if(confirm("Czy chcesz usunąć te wypożyczenie?"))
			{
				let input = document.createElement("input");
				let przycisk = document.createElement("button");
				input.value = wiersz;
				input.name = "wiersz";
				przycisk.type = "submit";
				formularz.appendChild(input);
				formularz.appendChild(przycisk);
				formularz.style.display = "none";
				formularz.submit();
			}
		}
	</script>
   
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    </main>
  </body>
</html>