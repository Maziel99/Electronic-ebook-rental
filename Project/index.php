<!DOCTYPE html>
<?php
    session_start();
?>
<html lang="PL-pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Internetowa Wypożyczalnia E-booków</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <header>
    <h1>Logowanie</h1>
    </header>
    <nav class="navbar sticky-top navbar-dark navbar-expand-lg" style="background-color: #444444;">
            <a class="navbar-brand" href="#">
            <div class="d-inline-block align-bottom baner"><img src="brand.png" alt="" height="50" width="50"></div>
                <div class="d-inline-block align-bottom baner">Internetowa <span class="title">Wypożyczalnia</span> E-booków</div>
            </a>
            <buttton class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#hambmenu" aria-controls="hambmenu" aria-expanded="false" aria-label="Navigation button">
                <span class="navbar-toggler-icon"></span>
            </buttton>
            <div class="collapse navbar-collapse justify-content-end" id="hambmenu">
                <div class="navbar-nav">
                    
                    <a class="nav-link" href="index.php">Logowanie</a>
                    
                    <a class="nav-link" href="rejestracja.php">Rejestracja</a>

                    <a class="nav-link" href="adminLogin.php">Administrator</a>
                </div>
            </div>
        </nav>
    <main>

<div class="container-fluid center_div">
<div class="col-12-md">
<form method="POST">
<div class="form-group">
  <div class="form-group">
    <label for="InputEmail">E-mail</label>
    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp"><br/>

  </div>
  <div class="form-group">
    <label for="password">Hasło</label>
    <input type="password" class="form-control" id="password" name="password"><br/>
  </div>
  <button type="submit" class="btn btn-primary">Zaloguj się</button>
  </div>
</form>
<?php
        if(isset($_POST['email'])){
            session_start();
            error_reporting(E_ALL);
            ini_set('display_errors', 'On');
                            
            include 'DBconnection.php'; 
            
            
            $email = $_POST['email'];
            $pass = $_POST['password'];


            $query = "begin
                :result := login('$email', '$pass');
                end;";
                        
            $c = oci_connect($username, $password, $database, `AL32UTF8`, OCI_SYSDBA);
            if (!$c) {
                $m = oci_error();
                trigger_error('Could not connect to database: '. $m['message'], E_USER_ERROR);
            }
                        
            $s = oci_parse($c, $query);
            if (!$s) {
                $m = oci_error($c);
                trigger_error('Could not parse statement: '. $m['message'], E_USER_ERROR);
            }
            oci_bind_by_name($s, ':result', $result, 100);
            oci_execute($s);
            echo $result;

            if($result == 'logged in'){
                $cookies = Array('email' => $email, 'password' => $pass);
                setcookie('cookies', serialize($cookies), time()+5000);
                if (isset($_COOKIE['cookies'])) $array = unserialize($_COOKIE['cookies']); 
                else $cookies = Array();

                $_SESSION['Authenticated'] = 1;
                session_write_close();
                
                
                header('Location: home.php');
            }
            else {
                session_destroy();
                header('Location: index.php');
            }
        }
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    </main>
  </body>
</html>