<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
            
    include "DBconnection.php";
	
    $autor = $_POST['autor'];

    $query = "BEGIN
    :RESULT := dodajautora('$autor');
    END;";
                
    $c = oci_connect($username, $password, $database, null, OCI_SYSDBA);
        if (!$c) {
        $m = oci_error();
        trigger_error('Could not connect to database: '. $m['message'], E_USER_ERROR);
    }
                
    $s = oci_parse($c, $query);
    if (!$s) {
        $m = oci_error($c);
        trigger_error('Could not parse statement: '. $m['message'], E_USER_ERROR);
    }
        
    oci_bind_by_name($s, ':RESULT', $result, 100);
    oci_execute($s);

    if($result){
        header("REFRESH:0.1; dodajAutora.php");
        echo "<script>alert('Dodano do bazy autora')</script>";
    }
    else {
        header("REFRESH:0.1, dodajAutora.php");
        echo "<script>alert('Wystąpił błąd')</script>";
    }
?>