<!DOCTYPE html>
<?php
    session_start();
?>
<html lang="PL-pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Internetowa Wypożyczalnia E-booków</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
    <header>
    <h1>Dodaj Ebook</h1>
    </header>
    <nav class="navbar sticky-top navbar-dark navbar-expand-lg" style="background-color: #444444;">
            <a class="navbar-brand" href="#">
            <div class="d-inline-block align-bottom baner"><img src="brand.png" alt="" height="50" width="50"></div>
                <div class="d-inline-block align-bottom baner">Internetowa <span class="title">Wypożyczalnia</span> E-booków</div>
            </a>
            <buttton class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#hambmenu" aria-controls="hambmenu" aria-expanded="false" aria-label="Navigation button">
                <span class="navbar-toggler-icon"></span>
            </buttton>
            <div class="collapse navbar-collapse justify-content-end" id="hambmenu">
            <div class="navbar-nav">

                    <a class="nav-link" href="adminHome.php">Strona główna</a>
                    
                    <a class="nav-link" href="index.php">Logowanie</a>
                    
                    <a class="nav-link" href="rejestracja.php">Rejestracja</a>

                    <?php
                        if(isset($_SESSION['Authenticated']) && ($_SESSION['Authenticated'] == 1)){
                    ?>
                    <a class="nav-link login" href="wylogowanie.php?wyloguj">Wylogowanie</a>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </nav>
    <main>

<div class="container-fluid center_div">
<div class="col-12-md">
<form action="dodawanieEbooka.php" method="POST">
<div class="form-group">

  <div class="form-group">
    <label for="id_konta">Tytuł</label>
    <input type="text" class="form-control" id="ksiazka" name="ksiazka" aria-describedby="emailHelp"><br/>

  </div>

  <div class="form-group">
    <label for="id_gatunku">ID Gatunku</label>
    <input type="number" class="form-control" id="gatunek" name="gatunek" aria-describedby="emailHelp"><br/>

  </div>

  <div class="form-group">
    <label for="id_autora">ID Autora</label>
    <input type="number" class="form-control" id="autor" name="autor" aria-describedby="emailHelp"><br/>

  </div>

  <div class="form-group">
    <label for="password">ID Wydawnictwa</label>
    <input type="number" class="form-control" id="wydawnictwo" name="wydawnictwo"><br/>
  </div>
  <button type="submit" class="btn btn-primary">Dodaj</button>
  </div>
</form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    </main>
  </body>
</html>