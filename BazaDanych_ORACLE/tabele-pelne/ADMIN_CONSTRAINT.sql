--------------------------------------------------------
--  Constraints for Table ADMIN
--------------------------------------------------------

  ALTER TABLE "SYS"."ADMIN" MODIFY ("ID_ADMINA" NOT NULL ENABLE);
  ALTER TABLE "SYS"."ADMIN" MODIFY ("EMAIL" NOT NULL ENABLE);
  ALTER TABLE "SYS"."ADMIN" MODIFY ("HASLO" NOT NULL ENABLE);
