REM INSERTING into SYS.AUTORZY
SET DEFINE OFF;
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('1','Stephen King');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('2','Stanislaw Lem');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('3','Philip K. Dick');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('4','Agata Christie');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('5','Chet Palakhniuk');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('6','Tom Clancy');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('7','Stephen Hawking');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('8','Roger Penrouse');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('9','David Attenborough');
Insert into SYS.AUTORZY (ID_AUTORA,IMIE_NAZWISKO) values ('10','Ryszard Kapuscinski');
