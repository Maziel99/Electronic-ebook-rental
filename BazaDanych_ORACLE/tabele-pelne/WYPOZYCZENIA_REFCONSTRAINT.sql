--------------------------------------------------------
--  Ref Constraints for Table WYPOZYCZENIA
--------------------------------------------------------

  ALTER TABLE "SYS"."WYPOZYCZENIA" ADD CONSTRAINT "WYPOZYCZENIA_FK1" FOREIGN KEY ("ID_KONTA")
	  REFERENCES "SYS"."KONTA" ("ID_KONTA") ENABLE;
  ALTER TABLE "SYS"."WYPOZYCZENIA" ADD CONSTRAINT "WYPOZYCZENIA_FK2" FOREIGN KEY ("ID_KSIAZKI")
	  REFERENCES "SYS"."EBOOKI" ("ID_KSIAZKI") ENABLE;
