--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function DODAJEBOOK
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."DODAJEBOOK" (
    tytul IN ebooki.tytul%TYPE,
    gatunek IN ebooki.id_gatunku%TYPE,
    autor IN ebooki.id_autora%TYPE,
    wydawnictwo IN ebooki.id_wydawnictwa%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(20);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_ksiazki) INTO i FROM ebooki;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
        END;
    INSERT INTO ebooki VALUES(i, tytul, gatunek, autor, wydawnictwo);
    alert := 'Dodano ebooka';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
