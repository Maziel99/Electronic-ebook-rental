--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function USUNKONTO
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."USUNKONTO" (
login IN konta.email%TYPE)
RETURN VARCHAR2
IS
  a VARCHAR2(26);
BEGIN
    BEGIN
        DELETE FROM KONTA WHERE email = login;
        a := 'usunieto';
    END;
    RETURN a;
END;

/
