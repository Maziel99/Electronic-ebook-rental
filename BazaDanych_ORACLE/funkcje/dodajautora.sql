--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function DODAJAUTORA
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."DODAJAUTORA" (
    autor IN autorzy.imie_nazwisko%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(20);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_autora) INTO i FROM autorzy;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
        END;
    INSERT INTO autorzy VALUES(i, autor);
    alert := 'Dodano autora';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
