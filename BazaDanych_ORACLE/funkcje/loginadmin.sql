--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function LOGINADMIN
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."LOGINADMIN" (login in varchar2, pass in varchar2)
RETURN VARCHAR2
AS
  logowanieAdmin NUMBER;
BEGIN
  SELECT COUNT(*)
    INTO logowanieAdmin
    FROM ADMINI
    WHERE email = login
    AND haslo = pass;
    IF logowanieAdmin = 0 THEN
        RETURN 'error';
    ELSIF logowanieAdmin = 1 THEN
        RETURN 'logged in';
    END IF;
END;

/
