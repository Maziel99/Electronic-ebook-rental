--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function DODAJGATUNEK
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."DODAJGATUNEK" (
    gatunek IN gatunki.nazwa%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(20);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_gatunku) INTO i FROM gatunki;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
        END;
    INSERT INTO gatunki VALUES(i, gatunek);
    alert := 'Dodano gatunek';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
