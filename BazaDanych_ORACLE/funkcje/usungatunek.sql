--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function USUNGATUNEK
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."USUNGATUNEK" (i IN gatunki.id_gatunku%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(30);
BEGIN
    BEGIN
        DELETE FROM gatunki WHERE id_gatunku = i;
        a := 'deleted';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
