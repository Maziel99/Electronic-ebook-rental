--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function USUNAUTORA
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."USUNAUTORA" (i IN autorzy.id_autora%TYPE)
RETURN VARCHAR2
IS
    a VARCHAR2(30);
BEGIN
    BEGIN
        DELETE FROM autorzy WHERE id_autora = i;
        a := 'deleted';
        EXCEPTION
            WHEN OTHERS THEN
            a := 'error';
        END;
        RETURN a;
END;

/
