--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function WYPOZYCZAJ
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."WYPOZYCZAJ" (
    i in konta.email%type,
    k in ebooki.id_ksiazki%type
)
return varchar2
is 
a varchar2(50);
BEGIN
    DECLARE
        idkonta integer :=0;
        idksiazki integer :=0;
        idwypozyczenia integer :=0;
    BEGIN
    a := '1';
        begin
            select konta.id_konta into idkonta from konta where konta.email=i;
        end;
        begin
            select ebooki.id_ksiazki into idksiazki from ebooki where ebooki.id_ksiazki=k;
        end;
        begin
        select MAX(id_wypozyczenia) into idwypozyczenia from wypozyczenia;
        idwypozyczenia := idwypozyczenia + 1;
        end;
        if k > 0 then
            begin
                BEGIN
                    insert into wypozyczenia values(idwypozyczenia, idkonta ,idksiazki, sysdate,sysdate+30);
                        a :='wypozyczono';
                exception
                        WHEN NO_DATA_FOUND THEN
                        a := '2';
                END;
            end;
        end if;
    end;
    commit;
    return a;
END;

/
