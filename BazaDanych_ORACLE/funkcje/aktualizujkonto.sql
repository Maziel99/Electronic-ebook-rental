--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function AKTUALIZUJKONTO
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."AKTUALIZUJKONTO" (
    login IN konta.email%TYPE,
    pass IN konta.haslo%TYPE)
RETURN VARCHAR2
is
  a VARCHAR2(26);
BEGIN
    BEGIN
        UPDATE KONTA 
        SET haslo = pass WHERE email = login;
        a := 'zaaktualizowano';
    END;
    RETURN a;
END;

/
