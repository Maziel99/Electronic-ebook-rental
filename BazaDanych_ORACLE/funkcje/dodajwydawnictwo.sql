--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function DODAJWYDAWNICTWO
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."DODAJWYDAWNICTWO" (
    wydawnictwo IN wydawnictwa.nazwa%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(20);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_wydawnictwa) INTO i FROM wydawnictwa;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
        END;
    INSERT INTO wydawnictwa VALUES(i, wydawnictwo);
    alert := 'Dodano autora';
    EXCEPTION
        WHEN OTHERS THEN
            alert := NULL;
    END;
    RETURN alert;
END;

/
