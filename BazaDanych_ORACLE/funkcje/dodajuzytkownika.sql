--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function DODAJUZYTKOWNIKA
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."DODAJUZYTKOWNIKA" (
    a IN konta.email%TYPE,
    b IN konta.haslo%TYPE)
RETURN VARCHAR2
IS alert VARCHAR2(200);
BEGIN
    DECLARE
        i integer := 0;
    BEGIN
        BEGIN
            SELECT MAX(id_konta) INTO i FROM KONTA;
            i := i + 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                i := 1;
    END;
    INSERT INTO KONTA VALUES(i, a, b);
    alert := 'Dodano konto';    
    END;
    RETURN alert;
END;

/
