--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function LOGIN
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE FUNCTION "SYS"."LOGIN" (login in varchar2, pass in varchar2)
RETURN VARCHAR2
AS
  logowanie NUMBER;
BEGIN
  SELECT COUNT(*)
    INTO logowanie
    FROM KONTA
    WHERE email = login
    AND haslo = pass;
    IF logowanie = 0 THEN
        RETURN 'error';
    ELSIF logowanie = 1 THEN
        RETURN 'logged in';
    END IF;
END;


/
