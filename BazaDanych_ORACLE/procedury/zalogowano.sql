--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure ZALOGOWANO
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."ZALOGOWANO" (
    login in konta.email%type,
    pass in konta.haslo%type)
as
cursor11 SYS_REFCURSOR;
BEGIN
      OPEN cursor11 FOR 
        SELECT konta.email
        FROM KONTA 
        WHERE haslo = pass;
        dbms_sql.return_result(cursor11);
END zalogowano;

/
