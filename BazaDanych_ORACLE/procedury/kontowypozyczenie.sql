--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure KONTOWYPOZYCZENIE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."KONTOWYPOZYCZENIE" (
    i in konta.email%TYPE)
AS
cursor13 SYS_REFCURSOR;
BEGIN
    OPEN cursor13 FOR
        SELECT konta.email id_konta,
        ebooki.tytul id_ksiazki, wypozyczenia.data_wypozyczenia, wypozyczenia.termin_wypozyczenia
        FROM wypozyczenia INNER JOIN konta ON wypozyczenia.id_konta = konta.id_konta
        INNER JOIN ebooki ON wypozyczenia.id_ksiazki = ebooki.id_ksiazki
        WHERE konta.email = i;
        dbms_sql.return_result(cursor13);
END kontowypozyczenie;

/
