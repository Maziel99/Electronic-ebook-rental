--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJEBOOKI
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJEBOOKI" 
AS
cursor1 SYS_REFCURSOR;
BEGIN
    OPEN cursor1 FOR
    SELECT  ebooki.id_ksiazki, ebooki.tytul, gatunki.nazwa id_gatunku,
    autorzy.imie_nazwisko id_autora, wydawnictwa.nazwa id_wydawnictwa
    FROM EBOOKI INNER JOIN GATUNKI ON ebooki.id_gatunku = gatunki.id_gatunku
    INNER JOIN AUTORZY ON ebooki.id_autora = autorzy.id_autora
    INNER JOIN WYDAWNICTWA ON ebooki.id_wydawnictwa = wydawnictwa.id_wydawnictwa
    ORDER BY ebooki.id_ksiazki;
    DBMS_SQL.RETURN_RESULT(cursor1);
END wczytajebooki;

/
