--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure ZALOGOWANOADMIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."ZALOGOWANOADMIN" (
    login in admini.email%type,
    pass in admini.haslo%type)
as
cursor15 SYS_REFCURSOR;
BEGIN
      OPEN cursor15 FOR 
        SELECT admini.email
        FROM ADMINI 
        WHERE haslo = pass;
        dbms_sql.return_result(cursor15);
END zalogowanoAdmin;

/
