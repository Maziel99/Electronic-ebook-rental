--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJAUTOROW
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJAUTOROW" 
AS
cursor3 SYS_REFCURSOR;
BEGIN
    OPEN cursor3 FOR
    SELECT * FROM AUTORZY
    ORDER BY id_autora;
    DBMS_SQL.RETURN_RESULT(cursor3);
END wczytajautorow;

/
