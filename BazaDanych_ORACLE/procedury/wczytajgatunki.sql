--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJGATUNKI
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJGATUNKI" 
AS
cursor2 SYS_REFCURSOR;
BEGIN
    OPEN cursor2 FOR
    SELECT * FROM GATUNKI
    ORDER BY id_gatunku;
    DBMS_SQL.RETURN_RESULT(cursor2);
END wczytajgatunki;

/
