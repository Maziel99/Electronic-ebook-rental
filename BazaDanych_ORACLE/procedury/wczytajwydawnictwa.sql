--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJWYDAWNICTWA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJWYDAWNICTWA" 
AS
cursor5 SYS_REFCURSOR;
BEGIN
    OPEN cursor5 FOR
    SELECT * FROM WYDAWNICTWA
    ORDER BY id_wydawnictwa;
    DBMS_SQL.RETURN_RESULT(cursor5);
END wczytajwydawnictwa;

/
