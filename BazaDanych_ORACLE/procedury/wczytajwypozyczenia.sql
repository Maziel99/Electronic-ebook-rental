--------------------------------------------------------
--  File created - czwartek-czerwca-10-2021   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure WCZYTAJWYPOZYCZENIA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "SYS"."WCZYTAJWYPOZYCZENIA" 
AS
cursor17 SYS_REFCURSOR;
BEGIN
    OPEN cursor17 FOR
        SELECT wypozyczenia.id_wypozyczenia, konta.email id_konta,
        ebooki.tytul id_ksiazki, wypozyczenia.data_wypozyczenia, wypozyczenia.termin_wypozyczenia
        FROM wypozyczenia INNER JOIN konta ON wypozyczenia.id_konta = konta.id_konta
        INNER JOIN ebooki ON wypozyczenia.id_ksiazki = ebooki.id_ksiazki;
        dbms_sql.return_result(cursor17);
END wczytajwypozyczenia;

/
